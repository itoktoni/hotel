
<script src="{{ Helper::backend('vendor/modernizr/modernizr.min.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery/jquery.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ Helper::backend('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ Helper::backend('vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-ui/js/jquery-ui.min.js') }}"></script>
<script src="{{ Helper::backend('vendor/select/select2.min.js') }}"></script>
<script src="{{ Helper::backend('vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-typeahead/jquery.typeahead.min.js') }}"></script>
<script src="{{ Helper::backend('vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>

<script src="{{ Helper::backend('vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ Helper::backend('vendor/pnotify/pnotify.custom.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>
<script src="{{ Helper::backend('javascripts/theme.custom.js') }}"></script>
<script src="{{ Helper::backend('javascripts/ui-elements/examples.modals.js') }}"></script>
@yield('js')