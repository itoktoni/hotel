<!doctype html>
<html class="fixed">
    <head>

        <meta charset="UTF-8">
        <meta name="keywords" content="{{ config('app.name', 'Laravel') }}" />
        <meta name="description" content="{{ config('app.name', 'Laravel') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('website.name', 'Laravel') }}</title>
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">-->
        <link rel="stylesheet" href="{{ asset('public/assets/vendor/bootstrap/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/vendor/font-awesome/css/font-awesome.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/stylesheets/theme.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/stylesheets/skins/default.css') }}" />
        <link rel="stylesheet" href="{{ asset('public/assets/stylesheets/theme-custom.css') }}">
        <script src="{{ asset('public/assets/vendor/modernizr/modernizr.js') }}"></script>

        <script> window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token(),]); ?></script>

    </head>
    <body>
        <!-- start: page -->
        <section class="body-sign body-locked">
            <div class="center-sign">
                <div class="panel panel-sign">
                    <div class="panel-body">
                        {!! Form::open(['route' => 'lock']) !!} 
                        <div class="current-user text-center">
                            @if(Auth::user()->photo == '')
                            <img src="{{ asset('public/assets/images/!logged-user.jpg') }}" alt="{{ Auth::user()->name }}" class="img-circle user-image" />
                            @else
                            <img src="{{ asset('public/files/profile/'.Auth::user()->photo) }}" alt="{{ Auth::user()->name }}" class="img-circle user-image" />
                            @endif
                            <h2 class="user-name text-dark m-none">{{ Auth::user()->name }}</h2>
                            <p class="user-email m-none">{{ Auth::user()->email }}</p>
                        </div>

                        @if ($errors->has('change_password'))
                        <span class="text-center help-block alert-danger">
                            <strong>{{ $errors->first('change_password') }}</strong>
                        </span>
                        @endif

                        @if(session()->has('sukses'))
                        <div class="text-center help-block alert-{{ session()->get('sukses') == true ? 'success' : 'danger' }}">
                            {{ session()->get('sukses') == true ? 'Password Has Been Change !' : 'Password Failed To Change !' }}
                        </div>
                        @endif

                        <div class="form-group mb-lg">
                            <div class="input-group input-group-icon">
                                <input id="pwd" type="password" name="change_password" class="form-control input-lg" placeholder="New Password" />
                                <input type="hidden" name="email" value="{{ Auth::User()->email }}">
                                <span class="input-group-addon">
                                    <span class="icon icon-lg">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <p class="mt-xs mb-none">
                                    <a href="{{ route('home') }}">Back To Home</a>
                                </p>
                            </div>
                            <div class="col-xs-6 text-right">
                                <button type="submit" class="btn btn-primary">Change</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
        <!-- end: page -->


        <!-- Vendor -->
        <script src="{{ asset('public/assets/vendor/jquery/jquery.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
        <script src="{{ asset('public/assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('public/assets/javascripts/theme.js') }}"></script>

        <!-- Theme Custom -->
        <script src="{{ asset('public/assets/javascripts/theme.custom.js') }}"></script>

        <!-- Theme Initialization Files -->
        <script src="{{ asset('public/assets/javascripts/theme.init.js') }}"></script>


    </body>
</html>