<head>
<title><?php echo config('website.name');?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="<?= config('website.description') ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{ Helper::frontend('styles/bootstrap4/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('styles/main_styles.css') }}">
<link rel="stylesheet" href="{{ Helper::frontend('styles/responsive.css') }}">
<link href="{{ Helper::files('logo/'.config('website.favicon')) }}" rel="icon">
</head>