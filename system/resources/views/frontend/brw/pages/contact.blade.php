@extends('frontend.'.config('website.frontend').'.layouts')
@section('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
<div class="contact">
		<div class="row">
			<div class="col">
				<h2 class="contact_title text-center">Paket Membership</h2>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d589.5177874384119!2d106.7954394459191!3d-6.291681432841428!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sGedung+Graha+Satria!5e0!3m2!1sen!2sid!4v1541783418162"frameborder="0" style="border:1px solid #b3585e;padding:10px;width: 100%;height: 100%;" allowfullscreen></iframe>
				</div>
				<div class="col-lg-5">
					<div class="contact_form_container">
						<div class="contact_title">get in touch</div>
						{!! Form::open(['route' => 'contact', 'class' => 'form-horizontal', 'files' => true]) !!}
							
							<input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Nama Depan" required="required" name="first_name" data-error="Subject is required.">

							<input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Nama Belakang" required="required" name="last_name" data-error="Subject is required.">
							
							<input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Email" required="required" name="email" data-error="Subject is required.">

							<input type="text" id="contact_form_subject" class="contact_form_subject input_field" placeholder="Subject" required="required" name="subject" data-error="Subject is required.">

							<input type="text" id="datepicker" class="contact_form_subject input_field" placeholder="Tanggal Lahir" name="date" required="required" data-error="Subject is required.">
							
							<select name="status" style="margin-top: 10px;height: 30px;font-size: 12px;" class="form-control" id="sel1">
							    <option>Single</option>
							    <option>Menikah</option>
							    <option>Cerai</option>
							  </select>

							 <select name="cc" style="margin-top: 10px;height: 30px;font-size: 12px;" class="form-control" id="sel1">
							    <option>Titanium</option>
							    <option>Platinum</option>
							    <option>Gold</option>
							    <option>Silver</option>
							  </select>

							<textarea name="message" id="contact_form_message" class="text_field contact_form_message" name="pesan" rows="4" placeholder="Pesan" required="required" data-error="Please, write us a message."></textarea>

							<button type="submit" id="form_submit_button" class="form_submit_button button">send message<span></span><span></span><span></span></button>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $( "#datepicker").datepicker({ dateFormat: "yy-mm-dd"});
</script>

@endsection