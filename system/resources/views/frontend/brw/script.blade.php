<script src="{{ Helper::frontend('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ Helper::frontend('styles/bootstrap4/popper.js') }}"></script>
<script src="{{ Helper::frontend('styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ Helper::frontend('plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{ Helper::frontend('plugins/easing/easing.js') }}"></script>
<script src="{{ Helper::frontend('js/custom.js') }}"></script>