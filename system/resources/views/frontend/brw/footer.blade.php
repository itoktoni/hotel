<!-- Footer -->

  <footer id="about" class="footer">
    <div class="container">
      <div class="row">
        <!-- Footer Column -->
        <div class="col-lg-9 footer_column">
          <div class="footer_col">
            <div class="footer_content footer_about">
              <div class="logo_container footer_logo">
                <div class="logo">
                  <a href="#">
                    <h2>About Us</h2>
                  </a>
                </div>
              </div>
              <?php echo config('website.description');?>
              <ul class="footer_social_list">
                <li class="footer_social_item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="footer_social_item"><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                <li class="footer_social_item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="footer_social_item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
                <li class="footer_social_item"><a href="#"><i class="fa fa-behance"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

        <!-- Footer Column -->
        

        <!-- Footer Column -->
        <div class="col-lg-3 footer_column">
          <div class="footer_col">
            <div class="footer_title">Kantor Pusat</div>
            <div class="footer_content footer_contact">
              <ul class="contact_info_list">
                <li class="contact_info_item d-flex flex-row">
                  <div><div class="contact_info_icon"><img src="{{ Helper::frontend('images/placeholder.svg') }}" alt=""></div></div>
                  <div class="contact_info_text">
                   <?php echo config('website.address');?>
                  </div>
                </li>
                <li class="contact_info_item d-flex flex-row">
                  <div><div class="contact_info_icon"><img src="{{ Helper::frontend('images/phone-call.svg') }}" alt=""></div></div>
                  <div class="contact_info_text"> <?php echo config('website.phone');?></div>
                </li>
                <li class="contact_info_item d-flex flex-row">
                  <div><div class="contact_info_icon"><img src="{{ Helper::frontend('images/message.svg') }}" alt=""></div></div>
                  <div class="contact_info_text"><a href="mailto:customerservice@brwholiday.com" target="_top">Customer Service</a></div>
                </li>
                <li class="contact_info_item d-flex flex-row">
                  <div><div class="contact_info_icon"><img src="{{ Helper::frontend('images/message.svg') }}" alt=""></div></div>
                  <div class="contact_info_text"><a href="mailto: reservation@brwholiday.com" target="_top">Reservation</a></div>
                </li>
                <li class="contact_info_item d-flex flex-row">
                  <div><div class="contact_info_icon"><img src="{{ Helper::frontend('images/planet-earth.svg') }}" alt=""></div></div>
                  <div class="contact_info_text"><a href="https://brwgoholiday.com">www.brwgoholiday.com</a></div>
                </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  </footer>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 order-lg-1 order-2  ">
          <div class="copyright_content d-flex flex-row align-items-center">
            <div>Copyright © <?= config('website.name') ?> 2018 All rights reserved </div>
          </div>
        </div>
        <div class="col-lg-7 order-lg-2 order-1">
          <div class="footer_nav_container d-flex flex-row align-items-center justify-content-lg-end">
            <div class="footer_nav">
              <ul class="footer_nav_list">
                <li class="footer_nav_item"><a href="index.html">home</a></li>
                <li class="footer_nav_item"><a href="#about">about us</a></li>
                <li class="footer_nav_item"><a href="index.html#offer">offers</a></li>
                <li class="footer_nav_item"><a href="contact.html">contact</a></li>
                <li class="footer_nav_item"><a style="color: #b3585e;" href="#">member area</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>