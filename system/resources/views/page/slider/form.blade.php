<div class="form-group">
    <label class="col-md-2 control-label">Slider Name</label>
    <div class="col-md-4 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
    
    <label class="col-md-2 control-label" for="inputDefault">Attachment</label>
    <div class="col-md-4">
        {!! Form::file('images', ['class' => 'btn btn-default btn-sm btn-block']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('name', 'Slider Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10 {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::textarea('description', null, ['class' => 'form-control summernote', 'rows' => '3']) !!}
    </div>
</div>