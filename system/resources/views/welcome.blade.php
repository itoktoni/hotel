<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    {{ config('app.name') }}
                </div>

               {!! Form::open(['route' => 'install', 'class' => 'form-horizontal', 'files' => true]) !!}  
                    

                <div class="links">
                    <div class="col-md-4">
                        <select class="form-control" name="provider">
                            <option {{ config('database.default') == 'sqlite' ? 'selected=selected' : '' }} value="sqlite"> Sqlite</option>
                            <option {{ config('database.default') == 'mysql' ? 'selected=selected' : '' }} value="mysql"> MySql</option>
                            <option {{ config('database.default') == 'pgsql' ? 'selected=selected' : '' }} value="pgsql"> PostgreSQL</option>
                        </select>
                    </div>
                   
                    <label class="col-md-2 control-label">Host</label>
                    <div class="col-md-4">
                        <input type="text" value="" name="host" class="form-control">
                    </div>

                    <label class="col-md-2 control-label">Database</label>
                    <div class="col-md-4">
                        <input type="text" value="" name="database" class="form-control">
                    </div>

                    <label class="col-md-2 control-label">Username</label>
                    <div class="col-md-4">
                        <input type="text" value="" name="username" class="form-control">
                    </div>

                    <label class="col-md-2 control-label">Password</label>
                    <div class="col-md-4">
                        <input type="password" value="" name="password" class="form-control">
                    </div>
                    <input type="submit" value="OK">
                    {{ Helper::asset('/system/data') }}

                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </body>
</html>
