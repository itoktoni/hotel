<?php

use Illuminate\Http\Request;
// use Helper;
// use Curl;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
//

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// Route::get('city', function(\App\City $model){
// 	$input = request()->get('q');
// 	$parent = request()->get('p');
// 	$query = DB::table($model->getTable());
// 	$query->select($model->getKeyName().' as id',$model->searching.' as text');
// 	$query->where($model->searching, "LIKE", "%{$input}%");
// 	if(!empty($parent)){
// 		$query->where('province_id',$parent);
// 	}
// 	$query->where($model->searching, "LIKE", "%{$input}%");
// 	return $query->get()->toJson();
// })->name('city');

// Route::get('district', function(\App\District $model){
// 	$input = request()->get('q');
// 	$parent = request()->get('p');
// 	$query = DB::table($model->getTable());
// 	$query->select($model->getKeyName().' as id',$model->searching.' as text');
// 	$query->where($model->searching, "LIKE", "%{$input}%");
// 	if(!empty($parent)){
// 		$query->where('city_id',$parent);
// 	}
// 	$query->where($model->searching, "LIKE", "%{$input}%");
// 	return $query->get()->toJson();
// })->name('district');

Route::post('customer','CustomerController@list')->name('customer-api');
Route::post('site','SiteController@list')->name('site-api');
Route::post('team','TeamController@list')->name('team-api');
Route::post('category','CategoryController@list')->name('category-api');
Route::post('grouping','GroupingController@list')->name('grouping-api');
Route::post('unit','UnitController@list')->name('unit-api');

Route::post('province','ProvinceController@list')->name('province-api');
Route::get('province',function(){
	$data = Helper::createOption('province-api');
	if(request()->get('p')){
		$province = request()->get('p');
		$data->where('province_id', $province);
	}
	$group = $data->map(function($item) {
		return ['id' => $item->province_id, 'text' => $item->province];
	});
	
	return json_encode($group->toArray());
})->name('province-api');

Route::post('city','CityController@list')->name('city-api');
Route::get('city',function(){
	$data = Helper::createOption('city-api');
	if(request()->get('p')){
		$key = request()->get('p');
		$data = $data->where('province_id', $key);
	}
	if(request()->get('q')){
		$name = request()->get('q');
		$data = $data->filter(function($element) use ($name) {
		     return false !== stristr($element->city_name, $name);
		});
	}
	$group = $data->map(function($item) {
		return ['id' => $item->city_id, 'text' => $item->city_name];
	})->values()->all();

	return json_encode($group);
})->name('city-api');

Route::post('district','DistrictController@list')->name('district-api');
Route::get('district',function(){
	$data = Helper::createOption('district-api');
	if(request()->get('p')){
		$key = request()->get('p');
		$data = $data->where('city_id', $key);
	}
	if(request()->get('q')){
		$name = request()->get('q');
		$data = $data->filter(function($element) use ($name) {
		     return false !== stristr($element->subdistrict_name, $name);
		});
	}
	$group = $data->map(function($item) {
		return ['id' => $item->subdistrict_id, 'text' => $item->subdistrict_name];
	})->values()->all();

	return json_encode($group);
})->name('district-api');