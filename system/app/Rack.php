<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model {

    protected $table      = 'racks';
    protected $primaryKey = 'rack_id';
    protected $fillable   = [
      'rack_id',
      'id_ruangan',
      'rack_name',
      'rack_description',
    ];
    public $searching    = 'rack_name';
    public $timestamps   = false;
    public $incrementing = false;
    public $rules        = [
      'rack_name' => 'required|min:3',
    ];
    public $datatable    = [
      'rack_id'         => 'Code',
      'gedung_name'  => 'Gedung',
      'ruangan_name'  => 'Ruangan',
      'rack_name'       => 'Rack Name',
    ];

    public function simpan($data)
    {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        $data = DB::table($this->table);
        $data->select(['racks.*','ruangan_name','gedung_name']);
        $data->leftjoin('ruangan', 'ruangan.ruangan_id','=','racks.id_ruangan');
        $data->leftjoin('gedung', 'ruangan.id_gedung','=','gedung.gedung_id');
        
        if(!empty($id))
        {
            $data->where($this->primaryKey, $id);
        }

        return $data;

    }

}
