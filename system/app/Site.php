<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Site extends Model{

    protected $table = 'sites';
    protected $primaryKey = 'site_id';
    protected $fillable = [
            'site_id',
            'site_name',
            'site_reference',
            'site_address',
            'site_contact_person',
            'site_contact_number',
            'site_account_number',
            'site_account_number',
            'site_account_reference'
    ];
    public $searching = 'site_name';
    public $timestamps = true;
    public $incrementing = false;
    public $rules = [
            'site_id' => 'required|unique:sites|min:3',
    ];
    public $datatable = [
            'site_id' => 'Code',
            'site_name' => 'Name',
            'site_address' => 'Address',
            'site_contact_person' => 'Contact',
            'site_contact_number' => 'Phone',
    ];

    public function simpan($data)
    {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            session()->put('danger', $e->getMessage());
        }
    }

    public function hapus($data)
    {

        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Illuminate\Database\QueryException $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function saveFilterSite($code, $data)
    {
        try
        {
            DB::table('filters')->where('key', '=', $code)->delete();
            if(!empty($data))
            {
                foreach($data as $d)
                {
                    DB::table('filters')->insert([
                            'key' => $code,
                            'value' => $d
                    ]);
                }
            }

            session()->put('success', 'Data Has Been Saved !');
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Illuminate\Database\QueryException $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function filterSite($id = null)
    {
        $data = DB::table($this->table);
        $data->select('sites.site_id', 'sites.site_name');
        $data->leftJoin('filters', 'sites.site_id', '=', 'filters.value');
        $data->leftJoin('users', 'users.site_id', '=', 'filters.key');
        if(!empty($id))
        {
            $data->where('users.user_id', '=', $id);
        }

        return $data;
    }

    public function getFilterSite($id = null){

        $data = DB::table($this->table);
        $data->select('sites.site_id', 'sites.site_name');
        $data->Join('filters', 'sites.site_id', '=', 'filters.value');
        if(!empty($id)){
            $data->where($this->primaryKey,'=',$id);
        }
        return $data;        
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return DB::table($this->table)->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

    public function getSite($id)
    {
         return $this->select()->join('filters','sites.site_id','=','filters.value')->where('key','=',$id);
    }

}
