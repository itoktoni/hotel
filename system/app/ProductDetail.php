<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model {

    protected $table      = 'product_details'; //nama table
    protected $primaryKey = 'product_detail_id'; //nama primary key
    protected $fillable   = [ //field yang mau di save ke database
        'product_detail_id',
        'product_detail_name',
        'created_at',
        'updated_at',
        'product_detail_images',
        'product_detail_harga_jual',
        'product_detail_harga_beli',
        'product_detail_master',
        'product_detail_stock',
        'product_detail_size',
        'product_detail_description'
    ];
    public $searching    = 'product_detail_name'; //default pencarian ketika di cari
    public $timestamps   = true; //kalau mau automatic update tanggal ketika save atau edit ubah jadi true
    public $incrementing = false; //kalau id nya mau dibuatkan otomatis dari laravel 1,2,3
    public $rules        = [ //validasi https://laravel.com/docs/5.5/validation
      'product_detail_id' => 'required|min:3',
      'product_detail_master' => 'required',
      'product_detail_size' => 'required',
    ];
    public $datatable    = [ //field dan header buat di show ke list table 
      'product_detail_id'   => 'ID Product',
      'product_detail_name' => 'Product Name',
      'product_detail_size' => 'Satuan',
      'product_detail_stock' => 'Stock',
      'product_detail_harga_jual' => 'Harga Jual',
      'product_detail_harga_beli' => 'Harga Beli',
    ];

    public function simpan($code, $request, $file = null)
    {
        try
        {
            if(!empty($file)) //handle images
            {
                $name   = $code . '.' . $file->extension();
                $simpen = $file->storeAs('product_detail', $name);

                $request['product_detail_images'] = $name;
            }
            
            $this->Create($request);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request, $file=null)
    {
        try
        {
            
            if(!empty($file)) //handle images
            {
                
                $name   = $id . '.' . $file->extension();
                $simpen = $file->storeAs('product_detail', $name);

                $request['product_detail_images'] = $name;
            }
            
            $s = $this->find($id);
            $s->update($request);
            
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return $this->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

}
