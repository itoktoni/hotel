<?php

namespace App\Http\Controllers;

use Auth;
use Curl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use Yajra\Datatables\Facades\Datatables;
use Helper;
use Yajra\Datatables\Datatables;

class CustomerController extends Controller
{

    public $table;
    public $key;
    public $field;
    public $model;
    public $template;
    public $rules;
    public $datatable;
    public $searching;
    public $render;

    public function __construct()
    {
        $this->model      = new \App\Customer();
        $this->table      = $this->model->getTable();
        $this->key        = $this->model->getKeyName();
        $this->field      = $this->model->getFillable();
        $this->datatable  = $this->model->datatable;
        $this->rules      = $this->model->rules;
        $this->searching  = $this->model->searching;
        $this->prefix     = "C" . date("m") . date("y");
        $this->codelength = 8;
        $this->template   = $this->getTemplate();
        $this->render     = 'page.' . $this->template . '.';
    }

    public function index()
    {
        return redirect()->route($this->getModule() . 'read');
    }

    private function share($data = null, $key = null)
    {

        $site      = new \App\Site();
        $user      = new \App\User();
        $permision = $user->baca()->where('group_user', 'sales');
        if (Auth::user()->group_user == 'sales') {

            $permision->where('email', '=', Auth::user()->email);
        }
        $view = [
            'site'     => $site->baca()->get(),
            'province' => json_decode(Curl::to(route('province'))->get()),
            'user'     => $permision->get(),
            'status'   => config('status.' . $this->table),
        ];
        $merge = array_merge($view, $data);
        // dd($merge);
        return $merge;
    }

    public function create()
    {
        if (request()->isMethod('POST')) {
            $this->validate(request(), $this->rules);
            $request = request()->all();
            // dd($request);
            $cek = $this->model->simpan($request);
            if ($cek) {
                return redirect()->back();
            }
        }

        $city = $district = [];
        if (request()->exists('province_id')) {
            $single_city = \App\City::single(request()->get('province_id'));
            $city        = $single_city->pluck('city_name', 'city_id');
        }

        if (request()->exists('city_id')) {
            $single_district = \App\District::single(request()->get('city_id'));
            $district        = $single_district->pluck('subdistrict_name', 'subdistrict_id');
        }

        return view($this->render . __function__)->with($this->share([
            'template' => $this->template,
            'model'    => $this->model,
            'city'     => $city,
            'district' => $district,

        ]));
    }

    function list() {
        if (request()->isMethod('POST')) {
            $getData   = $this->model->baca();
            $datatable = Datatables::of($this->filter($getData->latest()))
                ->addColumn('checkbox', function ($select) {
                    $id       = $this->key;
                    $checkbox = '<input type="checkbox" name="id[]" class="chkSelect" style="margin-left:10px;" id="checkbox1" value="' . $select->$id . '">';
                    return $checkbox;
                })->addColumn('customer_status', function ($select) {
                $flag   = '';
                $status = $select->customer_status;
                if ($status == 'ACTIVE') {
                    $flag = 'btn-info';
                } elseif ($status == 'PROSPECT') {
                    $flag = 'btn-warning';
                } elseif ($status == 'LEAD') {
                    $flag = 'btn-invers';
                }

                $checkbox = '<span class="btn ' . $flag . ' btn-xs btn-block">' . $select->customer_status . '</span>';
                return $checkbox;
            })->addColumn('action', function ($select) {
                $id     = $this->key;
                $gabung = '<div class="aksi text-center">';
                if (session()->get('akses.update')) {
                    $gabung = $gabung . '<a href="' . route($this->getModule() . '_update', [
                        'code' => $select->$id]) . '" class="btn btn-xs btn-primary">edit</a> ';
                }
                $gabung = $gabung . ' <a href="' . route(Route::currentRouteName(), [
                    'code' => $select->$id]) . '" class="btn btn-xs btn-success">show</a></div>';
                return $gabung;
            });

            if (request()->has('search')) {
                $code      = request()->get('code');
                $search    = request()->get('search');
                $aggregate = request()->get('aggregate');
                $datatable->where(empty($code) ? $this->searching : $code, empty($aggregate) ? 'like' : $aggregate, "%$search%");
            }

            return $datatable->make(true);
        }
        if (request()->has('code')) {
            $id   = request()->get('code');
            $data = $this->model->baca($id);

            return view($this->render . 'show')->with([
                'fields'   => Helper::listData($this->datatable),
                'data'     => $this->validasi($data),
                'key'      => $this->key,
                'template' => $this->template,
            ]);
        }

        return view($this->render . __function__)->with([
            'fields'   => Helper::listData($this->datatable),
            'template' => $this->template,
        ]);
    }

    public function update()
    {
        $id = request()->get('code');
        if (request()->isMethod('POST')) {
            $request = request()->all();
            $this->model->ubah($id, $request);
            return redirect()->route($this->getModule() . '_list');
        }

        $getData = $this->model->baca($id);
        $data    = $this->validasi($getData);
        $city    = $district    = [];

        $single_city = \App\City::single($data->province_id);
        $city        = $single_city->pluck('city_name', 'city_id');

        $single_district = \App\District::single($data->city_id);
        $district        = $single_district->pluck('subdistrict_name', 'subdistrict_id');

        return view($this->render . __function__)->with($this->share([
            'template' => $this->template,
            'model'    => $data,
            'key'      => $this->key,
            'city'     => $city,
            'district' => $district,
        ]));

    }

    public function delete()
    {
        $input = request()->all();
        $this->model->hapus($input);

        return redirect()->back();
    }

}
