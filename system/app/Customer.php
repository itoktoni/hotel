<?php

namespace App;

use Auth;
use Helper;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table      = 'customer'; //nama table
    protected $primaryKey = 'id'; //nama primary key
    protected $fillable = [
        "id",
        "name",
        "phone",
        "contact",
        "status",
        "email",
        "description",
        "address",
        "created_by",
        "updated_by",
        "email",
        "created_at",
        "updated_at",
        "site_id",
        "city_id",
        "province_id",
        "subdistrict_id",
    ];
    public $datatable = [
        'id'       => [true => 'ID Category'],
        'name'     => [true => 'Customer Name'],
        'email'    => [true => 'Email'],
        'status'   => [true => 'Status'],
    ];
    public $searching     = 'name'; //default pencarian ketika di cari
    public $timestamps    = true; //kalau mau automatic update tanggal
    public $incrementing  = false; //kalau id nya mau dibuatkan otomatis
    public $rules         = [ //validasi https://laravel.com/docs/5.5/validation
        'name'    => 'required|min:3',
        'email'   => 'email',
    ];

    public $status = [
        'ACTIVE'   => ['ACTIVE', 'success'],
        'PROSPECT' => ['PROSPECT', 'primary'],
        'INACTIVE' => ['INACTIVE', 'danger'],
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected function generateKey()
    {
        $autonumber = 'C' . date('Y') . date('m');
        return Helper::code($this->table, $this->primaryKey, $autonumber, config('website.autonumber'));
    }

    public static function boot()
    {
        static::updating(function ($table) {
            $table->updated_by = Auth::user()->username;
        });

        static::saving(function ($table) {
            $table->created_by = Auth::user()->username;
        });
    }

    public function simpan($request)
    {
        try
        {
            if (!$this->incrementing) {
                $code                       = $this->generateKey();
                $request[$this->primaryKey] = $code;
            }
            $activity = $this->create($request);
            if ($activity->save()) {
                session()->put('success', 'Data Has Been Added !');
                return true;
            }

            session()->put('danger', 'Data Failed To Save !');
            return false;

        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function hapus($data)
    {
        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try
            {
                $activity = $this->Destroy($data);
                if ($activity) {
                    session()->put('success', 'Data Has Been Deleted !');
                    return true;
                }
                session()->flash('alert-danger', 'Data Can not Deleted !');
                return false;
            } catch (\Illuminate\Database\QueryException $ex) {
                session()->flash('alert-danger', $ex->getMessage());
            }
        }
    }

    public function ubah($id, $request)
    {
        try
        {
            $activity = $this->find($id)->update($request);
            if ($activity) {
                session()->put('success', 'Data Has Been Updated !');
            }

            return $activity;

        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function baca($id = null)
    {
        if (!empty($id)) {
            $data = $this->find($id);
            return $data;
        }

        $model = $this->select(Helper::fields($this->datatable));
        return $model;
    }

}
