<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PurcaseOrder extends Model {

    protected $table = 'purchase_orders';
    protected $primaryKey = 'purchase_id';
    protected $fillable = [
        'purchase_id',
        'purchase_reff',
        'purchase_delivery',
        'purchase_invoice',
        'purchase_date',
        'purchase_delivery_date',
        'purchase_receive_date',
        'purchase_invoice_date',
        'email',
        'purchase_type_delivery',
        'purchase_note',
        'customer_id',
        'purchase_address',
        'address_recid',
        'site_id',
        'created_by',
        'purchase_status',
        'purchase_attachment',
        'purchase_note_supplier',
        'supplier_id',
        'warehouse_id',
    ];
    public $searching = 'purchase_id';
    public $timestamps = true;
    public $incrementing = false;
    public $rules = [
        'supplier_id' => 'required',
        'warehouse_id' => 'required',
    ];
    public $datatable = [
        'purchase_id' => 'No purchase',
        'purchase_date' => 'Tgl purchase',
        'purchase_delivery_date' => 'Tgl Pengiriman',
        'supplier_name' => 'Name Customer',
        'purchase_status' => 'Status',
    ];

    public function detail($data) {
        try {
            DB::table('purchase_order_detail')->insert($data);
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function simpan($code, $request, $file) {
        try {
            if (!empty($file)) {
                $name = $code . '.' . $file->extension();
                $simpen = $file->storeAs('po', $name);
                $request['purchase_attachment'] = $name;
            }
            
            $request['purchase_id'] = $code;
            $request['purchase_status'] = 'OPEN';
            $request['purchase_date'] = date("Y-m-d");
            $request['created_by'] = Auth::user()->username;

            $this->Create($request);

            session()->put('success', 'No.' . $code . " Berhasil Disimpan");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data) {

        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            } catch (\Exception $e) {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $request,$file=null)
    {
        try
        {
            if (!empty($file)) {
                $name = $id . '.' . $file->extension();
                $simpen = $file->storeAs('po', $name);
                $request['purchase_attachment'] = $name;

                if($request['purchase_status'] == 'OPEN'){
                    $request['purchase_status'] = 'APPROVED';
                }
            }

            $s = $this->find($id);
            $s->update($request);

            $form = $request['form'];
            if(isset($request['product'])){

                $item = $request['product'];
                if($form == 'approve'){
                    $quantity = $request['qty'];
                    $harga = $request['price'];

                    for($i=0; $i < count($item); $i++){

                        $qty = $quantity[$i];
                        $price = $harga[$i];
                        $product = $item[$i];
                        $total = $qty * $price;

                        DB::table('purchase_order_detail')
                        ->where(['detail' => $id, 'product' => $product])
                        ->update(['qty' => $qty, 'price' => $price, 'total' => $total]);
                    }
                }
                else if($form == 'receive'){

                    $quantity = $request['receive'];

                    for($i=0; $i < count($item); $i++){

                        $qty = $quantity[$i];
                        $product = $item[$i];

                        $total = DB::table('purchase_order_detail');
                        $total->where(['detail' => $id, 'product' => $product]);
                        $d_total = $total->get()->first();
                        $total->update([
                            'qty_receive' => $qty,
                            'price_receive' => $d_total->price,
                            'total_receive' => $qty * $d_total->price,
                        ]);

                        if($request['purchase_status'] == 'RECEIVED'){
                            $pro = DB::table('products');
                            $pro->where('product_id', $product);

                            $dp = $pro->get()->first();
                            $pro->update(['product_stock' => $dp->product_stock + $qty]);

                        }
                    }
                }
                else if($form == 'delivery'){

                    $quantity = $request['qty'];
                    $harga = $request['price'];
                    for($i=0; $i < count($item); $i++){

                        $qty = $quantity[$i];
                        $price = $harga[$i];
                        $product = $item[$i];
                        $total = $qty * $price;

                        DB::table('purchase_order_detail')
                        ->where(['detail' => $id, 'product' => $product])
                        ->update(['qty_prepare' => $qty, 'price_prepare' => $price, 'total_prepare' => $total]);
                    }
                }
            }
            
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function cancel($data) {
        try {
            foreach ($data as $a) {
                $dapet = $this->select('purchase_status')->where($this->primaryKey, '=', $a)->get();
                $status = $dapet->first();
                if ($status->purchase_status == 'OPEN' || $dapet->purchase_status == 'APPROVED') {
                    $this->where($this->primaryKey, $a)->update(['purchase_status' => 'CANCEL']);
                    session()->flash('alert-success', 'purchase Has Been Cancel !');
                }
            }
        } catch (\Exception $e) {
            session()->flash('alert-danger', 'Cannot Cancel This purchase');
        }
    }

    public function getByTop() {
        return $this->select('customer_id', 'customer_name');
    }

    public function getDetail($id) {

        $select = DB::table('purchase_order_detail');
        $select->join('purchase_orders', 'purchase_id', '=', 'purchase_order_detail.detail');
        $select->join('products', 'products.product_id', '=', 'purchase_order_detail.product');

        return $select->where($this->primaryKey, $id)->get();
    }

    public function getReceive($id) {

        $select = DB::table('purchase_order_detail');
        $select->select(['purchase_order_detail.*','products.*',DB::raw('sum(stocks.qty) as receive')]);
        $select->leftjoin('stocks', 'stocks.product_code', '=', 'purchase_order_detail.product');
        $select->join('products', 'products.product_id', '=', 'purchase_order_detail.product');
        $select->GroupBy('purchase_order_detail.product');
        return $select->where('detail', $id)->get();
    }

    public function baca($id = null) {
        $select = $this->select([
            'purchase_orders.*',
            'supplier_name',
            'supplier_bank_person',
            'supplier_bank_name',
            'supplier_bank_place',
            'supplier_bank_account',
            'supplier_alamat',
            'supplier_telp',
            'email',
            'supplier_owner',
        ]);
        $select->join('suppliers', 'purchase_orders.supplier_id', '=', 'suppliers.supplier_id');

        if (!empty($id)) {
            $select->where($this->primaryKey, $id);
        }

        return $select;
    }

}
