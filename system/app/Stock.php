<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class Stock extends Model{

    protected $table = 'stocks';
    protected $primaryKey = 'barcode';
    protected $fillable = [
        'barcode',
        'reference',
        'product_code',
        'rack_code',
        'qty',
        'date_in',
        'date_out',
        'wh_type',
        'created_by',
        'updated_by',
        'valid',
        'checked_by',
        'checked_at',
    ];
    public $searching = 'product_name';
    public $timestamps = true;
    public $incrementing = false;
    public $rules = [
        'barcode' => 'required|unique:stocks|min:3',
        'product_code' => 'required|min:3',
        'qty' => 'required',
    ];
    public $datatable = [
        'product_id' => 'Product ID',
        'product_category' => 'Category',
        'product_material' => 'Material',
        'product_size' => 'Size',
        'product_name' => 'Product Full Name',
        'product_stock' => 'Stock',
        'product_unit' => 'Unit',
    ];

    public function simpan($request) {
        try {

            $request['wh_type'] = 'IN';
            $request['date_in'] = date("Y-m-d H:i:s");
            $request['created_by'] = Auth::user()->username;

            $this->Create($request);

            session()->put('success', 'No.' . $request['product_code'] . " Berhasil Disimpan");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function out($request) {
        try {
            $data['wh_type'] = 'IN';
            $data['date_out'] = date("Y-m-d H:i:s");
            $data['reference'] = $request['reference'];
            $data['product_code'] = $request['product_code'];
            $data['qty'] = $request['qty'];
            $s = $this->where('barcode', $request['barcode']);
            $s->update($data);

            session()->put('success', 'No.' . $data['product_code'] . " Berhasil Disimpan");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function opname($request) {
        try {
            $data['valid'] = '1';
            $data['checked_at'] = date("Y-m-d H:i:s");
            $data['checked_by'] = Auth::user()->name;
            $s = $this->where('barcode', $request['barcode']);
            $s->update($data);

            session()->put('success', 'No.' . $request['barcode'] . " Berhasil Di Opname");
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function barcode($data) {
        try {
            DB::table('barcode')->insert($data);
        } catch (Exception $ex) {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function getBySales()
    {
        $data = DB::table('products');
        return $data;
    }

    public function getBySupplier()
    {
        $data = DB::table('products');
        $data->select(['product_id','purchase_delivery_date','product_name','product_size','qty_prepare','product_unit','supplier_name']);
        $data->join('purchase_order_detail','products.product_id','=','purchase_order_detail.product');
        $data->join('purchase_orders','purchase_orders.purchase_id','=','purchase_order_detail.detail');
        $data->join('suppliers','purchase_orders.supplier_id','=','suppliers.supplier_id');
        $data->Where('purchase_status','=','PREPARED');
        $data->orWhere('purchase_status','=','DELIVERED');
        return $data;
    }

    public function getBySPK()
    {
        $data = DB::table('products');
        $data->select(['product_id','spk_delivery_date','product_name','product_size','qty_prepare','product_unit','production_name']);
        $data->join('spk_detail','products.product_id','=','spk_detail.product');
        $data->join('spk','spk.spk_id','=','spk_detail.detail');
        $data->join('productions','spk.production_id','=','productions.production_id');
        $data->Where('spk_status','=','PREPARED');
        $data->orWhere('spk_status','=','DELIVERED');
        return $data;
    }

    public function getByReal()
    {
        $data = DB::table('stocks');
        $data->select(['products.*',DB::raw('(SUM(stocks.qty)) as jumlah'),'stocks.reference']);
        $data->Join('products','products.product_id','=','stocks.product_code');
        $data->where('stocks.wh_type','=','IN');
        $data->groupBy('stocks.product_code');
        return $data;
    }

    public function getDetailRack($product)
    {
        $data = DB::table('stocks');
        $data->leftJoin('racks','racks.rack_id','=','stocks.rack_code');
        $data->leftJoin('warehouses','racks.warehouse_id','=','warehouses.warehouse_id');
        $data->where('stocks.wh_type','=','IN');
        $data->where('stocks.product_code','=',$product);

        return $data;
    }

    public function getRevisi($barcode)
    {
        $data = DB::table('stocks');
        $data->leftJoin('racks','racks.rack_id','=','stocks.rack_code');
        $data->where('stocks.barcode','=',$barcode);
        
        return $data;
    }

    public function ubah($id, $request)
    {
        try
        {   
            $s = $this->find($id);
            $s->update($request);
            
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        $data = DB::table('stocks');
        if(!empty($id))
        {
            $data->where('barcode', $id);
        }

        return $data->select()->join('products','products.product_id','=','stocks.product_code');
    }

    public function getBarcode($id = null)
    {
        $data = DB::table('barcode');
        if(!empty($id))
        {
            $data->where('unic', $id);
        }

        return $data->select()->join('products','products.product_id','=','barcode.product');
    }

}
