<?php
namespace App;

use Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Unit extends Model
{
    protected $table      = 'units';
    protected $primaryKey = 'unit_id';
    protected $fillable   = [
    ];

    public static $autonumber;
    public $datatable;

    protected $guarded   = [];
    public $searching    = 'unit_name';
    public $timestamps   = true;
    public $incrementing = false;
    public $rules        = [
        'unit_name' => 'required|min:3',
    ];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function __construct($attributes = [])
    {
        parent::__construct($attributes); 
        $this->fillable = Helper::getTable($this->table);
        $this->datatable = [
            'unit_id'      => [true => 'ID unit'],
            'unit_name'    => [true => 'Name'],
        ];
    }


    protected function generateKey()
    {
        $autonumber = 'C' . date('Y') . date('m');
        return Helper::code($this->table, $this->primaryKey, $autonumber, config('website.autonumber'));
    }

    public function simpan($request)
    {
        try
        {
            $activity = $this->create($request);
            if ($activity->save()) {
                session()->put('success', 'Data Has Been Added !');
                return true;
            }
        } catch (\Illuminate\Database\QueryException $ex) {

            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try
            {
                $activity = $this->Destroy($data);
                if ($activity) {
                    session()->put('success', 'Data Has Been Deleted !');
                    return true;
                }
                session()->flash('alert-danger', 'Data Can not Deleted !');
                return false;
            } catch (\Illuminate\Database\QueryException $ex) {
                session()->flash('alert-danger', $ex->getMessage());
            }
        }
    }

    public function ubah($id, $request)
    {
        try
        {
            $activity = $this->find($id)->update($request);
            if ($activity) {
                session()->put('success', 'Data Has Been Updated !');
            }

            return $activity;
        } catch (\Illuminate\Database\QueryException $ex) {
            session()->flash('alert-danger', $ex->getMessage());
        }
    }

    public function baca($id = null)
    {
        if (!empty($id)) {
            return $this->find($id);
        } 

        $model = $this->select(Helper::fields($this->datatable));
        return $model;
    }

}
