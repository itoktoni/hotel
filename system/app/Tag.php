<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

        protected $table      = 'tags';
        protected $primaryKey = 'tag_id';
        protected $fillable   = [
          'tag_id',
          'tag_name',
          'tag_description',
          'created_at',
          'updated_at',
      ];
      // protected $fillable;
      public $searching    = 'tag_name';
      public $timestamps   = true;
      public $incrementing = false;
      public $rules        = [
          'tag_name' => 'required|min:3',
      ];
      public $datatable    = [
          'tag_id'       => 'Code',
          'tag_name'       => 'Tag Name',
      ];

      public function simpan($data)
      {
        try
        {
            $model = new static($data);
            $check = $model->save();
            session()->put('success', 'Data Has Been Created');
            return $check;
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Illuminate\Database\QueryException $ex)
            {
                session()->flash('alert-danger', $ex->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
            return $s;
        }
        catch(\Illuminate\Database\QueryException $ex)
        {
            session()->flash('alert-danger', $ex->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return DB::table($this->table)->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

}
