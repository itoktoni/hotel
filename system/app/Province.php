<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;
use Auth;

class Province extends Model
{
    protected $table      = 'provinces'; //nama table
    protected $primaryKey = 'province_id'; //nama primary key
    protected $guarded = [];
    public $searching    = 'province'; //default pencarian ketika di cari
    public $timestamps   = false; //kalau mau automatic update tanggal
    public $incrementing = true; //kalau id nya mau dibuatkan otomatis
    public $rules        = [ //validasi https://laravel.com/docs/5.5/validation
        'province'    => 'required|min:3',
    ];
    public $datatable;
    public static $autonumber; 
    public $status = [
        // 'ACTIVE' => 'ACTIVE',
        // 'PROSPECT' => 'PROSPECT',
        // 'INACTIVE' => 'INACTIVE',
    ];

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';
    // protected $dates = [
    //     'created_at',
    //     'updated_at',
    // ];

   public function __construct($attributes = [])
    {
        parent::__construct($attributes); 
        $this->fillable = Helper::getTable($this->table);
        $this->datatable =  [
            'province_id'      => [true => 'ID province'],
            'province'    => [true => 'Province Name'],
        ];
    }

    protected function generateKey()
    {
        $autonumber = 'C' . date('Y') . date('m');
        return Helper::code($this->table, $this->primaryKey, $autonumber, config('website.autonumber'));
    }

    public static function boot()
    {
        // static::updating(function ($table) {
        //     $table->updated_by = Auth::user()->username;
        // });

        // static::saving(function ($table) {
        //     $table->created_by = Auth::user()->username;
        // });
    }

    public function simpan($request)
    {
        try
        {
            if(!$this->incrementing){
                $code = $this->generateKey();
                $request[$this->primaryKey] = $code;
            }
            $activity = $this->create($request);
            if ($activity->save()) {
                session()->put('success', 'Data Has Been Added !');
                return true;
            } 

            session()->put('danger', 'Data Failed To Save !');
            return false;
            
        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function hapus($data)
    {
        if (!empty($data)) {
            $data = collect($data)->flatten()->all();
            try
            {
                $activity = $this->Destroy($data);
                if ($activity) {
                    session()->put('success', 'Data Has Been Deleted !');
                    return true;
                } 
                session()->flash('alert-danger', 'Data Can not Deleted !');
                return false;
            } catch (\Illuminate\Database\QueryException $ex) {
                session()->flash('alert-danger', $ex->getMessage());
            }
        }
    }

    public function ubah($id, $request)
    {
        try
        {
            $activity = $this->find($id)->update($request);
            if ($activity) {
                session()->put('success', 'Data Has Been Updated !');
            } 

            return $activity;

        } catch (\Illuminate\Database\QueryException $ex) {
            session()->put('danger', $ex->getMessage());
            return false;
        }
    }

    public function baca($id = null)
    {
        if (!empty($id)) {
            $data = $this->find($id);
            return $data;
        }

        $model = $this->select(Helper::fields($this->datatable));
        return $model;
    }

}
