<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model {

        protected $table      = 'buku';
        protected $primaryKey = 'buku_id';
        protected $fillable   = [
        'buku_id',
        'buku_nama',
        'buku_penulis',
        'buku_penerbit',
        'buku_isbn',
        'buku_keterangan',
        'buku_stock',
        'category_buku',
        'buku_tag',
        'buku_tahun',
        'created_at',
        'updated_at',
      ];
      public $searching    = 'buku_nama';
      public $timestamps   = true;
      public $incrementing = false;
      public $rules        = [
          'buku_nama' => 'required|min:3',
      ];
      public $datatable    = [
          'buku_id'       => 'Code',
          'buku_nama'       => 'buku Name',
      ];

      public function simpan($data)
      {
        try
        {
            $this->Create($data);
            session()->put('success', 'Data Has Been Added !');
        }
        catch(Exception $ex)
        {
            session()->put('danger', $ex->getMessage());
        }
    }

    public function hapus($data)
    {
        if(!empty($data))
        {
            $data = collect($data)->flatten()->all();
            try
            {
                $this->Destroy($data);
                session()->flash('alert-success', 'Data Has Been Deleted !');
            }
            catch(\Exception $e)
            {
                session()->flash('alert-danger', $e->getMessage());
            }
        }
    }

    public function ubah($id, $data)
    {
        try
        {
            $s = $this->find($id);
            $s->update($data);
            session()->put('success', 'Data Has Been Updated !');
        }
        catch(\Exception $e)
        {
            session()->flash('alert-danger', $e->getMessage());
        }
    }

    public function baca($id = null)
    {
        if(!empty($id))
        {
            return DB::table($this->table)->where($this->primaryKey, $id);
        }
        else
        {
            return $this->select();
        }
    }

}
