<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDistrictsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('districts', function(Blueprint $table)
		{
			$table->integer('subdistrict_id')->unique('subdistrict_id');
			$table->integer('province_id')->nullable();
			$table->string('province')->nullable();
			$table->integer('city_id')->nullable();
			$table->string('city')->nullable();
			$table->string('type')->nullable();
			$table->string('subdistrict_name')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('districts');
	}

}
