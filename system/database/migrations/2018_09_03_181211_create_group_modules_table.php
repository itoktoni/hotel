<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGroupModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_modules', function(Blueprint $table)
		{
			$table->string('group_module_code')->unique('group_module_code');
			$table->string('group_module_name');
			$table->text('group_module_description')->nullable();
			$table->string('group_module_link')->nullable();
			$table->integer('group_module_sort')->nullable()->default(0);
			$table->enum('group_module_visible', array('1','0'))->nullable()->default('1');
			$table->enum('group_module_enable', array('1','0'))->nullable()->default('1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('group_modules');
	}

}
