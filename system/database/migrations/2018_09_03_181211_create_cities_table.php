<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table)
		{
			$table->integer('city_id')->unique('city_id');
			$table->integer('province_id')->nullable();
			$table->string('province')->nullable();
			$table->string('type')->nullable();
			$table->string('city_name')->nullable();
			$table->integer('postal_code')->nullable()->index('postal_code');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
