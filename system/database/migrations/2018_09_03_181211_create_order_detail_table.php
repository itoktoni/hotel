<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_detail', function(Blueprint $table)
		{
			$table->string('detail');
			$table->string('product');
			$table->float('qty', 20, 3)->nullable();
			$table->integer('suggestion')->nullable();
			$table->integer('price')->nullable();
			$table->integer('total')->nullable();
			$table->integer('line')->nullable();
			$table->float('qty_prepare', 20)->nullable();
			$table->integer('price_prepare')->nullable();
			$table->integer('total_prepare')->nullable();
			$table->float('qty_release', 20)->nullable();
			$table->integer('price_release')->nullable();
			$table->integer('total_release')->nullable();
			$table->primary(['detail','product']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_detail');
	}

}
