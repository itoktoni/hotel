<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->string('order_id', 50)->unique('order_id');
			$table->string('order_reff')->nullable();
			$table->string('order_delivery')->nullable();
			$table->string('order_invoice')->nullable();
			$table->date('order_date')->nullable();
			$table->date('order_delivery_date')->nullable();
			$table->dateTime('order_invoice_date')->nullable();
			$table->string('email')->nullable();
			$table->string('order_attachment')->nullable()->default('');
			$table->text('order_note')->nullable();
			$table->timestamps();
			$table->string('created_by')->nullable();
			$table->string('customer_id')->nullable();
			$table->text('order_address')->nullable();
			$table->string('order_status')->nullable()->index('order_status');
			$table->integer('province_to')->nullable();
			$table->integer('city_to')->nullable();
			$table->integer('province_from')->nullable();
			$table->integer('city_from')->nullable();
			$table->integer('delivery_cost')->nullable();
			$table->integer('estimasi_cost')->nullable();
			$table->integer('sales_cost')->nullable();
			$table->integer('finance_cost')->nullable();
			$table->float('qty_total', 50)->nullable();
			$table->string('courier')->nullable();
			$table->string('courier_service')->nullable();
			$table->string('airbill')->nullable();
			$table->integer('district_to')->nullable();
			$table->integer('district_from')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
