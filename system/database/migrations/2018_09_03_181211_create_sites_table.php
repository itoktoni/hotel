<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sites', function(Blueprint $table)
		{
			$table->string('site_id', 20)->unique('site_id');
			$table->string('site_name');
			$table->string('site_reference')->nullable();
			$table->text('site_address')->nullable();
			$table->string('site_contact_person')->nullable();
			$table->string('site_contact_number')->nullable();
			$table->string('site_account_number')->nullable();
			$table->string('site_account_reference')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sites');
	}

}
