<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->string('user_id', 50)->index('user_id');
			$table->string('nik')->nullable();
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password')->nullable();
			$table->string('username')->nullable();
			$table->string('photo')->nullable();
			$table->boolean('active')->nullable();
			$table->string('group_user')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->string('warehouse')->nullable();
			$table->string('site_id')->nullable();
			$table->float('target', 100, 0)->nullable();
			$table->float('pencapaian', 100, 0)->nullable();
			$table->string('gender', 1)->nullable();
			$table->text('address')->nullable();
			$table->date('birth')->nullable();
			$table->string('place_birth')->nullable();
			$table->text('biografi')->nullable();
			$table->string('handphone')->nullable();
			$table->string('no_tax')->nullable();
			$table->string('created_by')->nullable();
			$table->string('sales_responsible')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
