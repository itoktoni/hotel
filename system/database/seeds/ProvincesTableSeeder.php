<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'province_id' => 1,
                'province' => 'Bali',
            ),
            1 => 
            array (
                'province_id' => 2,
                'province' => 'Bangka Belitung',
            ),
            2 => 
            array (
                'province_id' => 3,
                'province' => 'Banten',
            ),
            3 => 
            array (
                'province_id' => 4,
                'province' => 'Bengkulu',
            ),
            4 => 
            array (
                'province_id' => 5,
                'province' => 'DI Yogyakarta',
            ),
            5 => 
            array (
                'province_id' => 6,
                'province' => 'DKI Jakarta',
            ),
            6 => 
            array (
                'province_id' => 7,
                'province' => 'Gorontalo',
            ),
            7 => 
            array (
                'province_id' => 8,
                'province' => 'Jambi',
            ),
            8 => 
            array (
                'province_id' => 9,
                'province' => 'Jawa Barat',
            ),
            9 => 
            array (
                'province_id' => 10,
                'province' => 'Jawa Tengah',
            ),
            10 => 
            array (
                'province_id' => 11,
                'province' => 'Jawa Timur',
            ),
            11 => 
            array (
                'province_id' => 12,
                'province' => 'Kalimantan Barat',
            ),
            12 => 
            array (
                'province_id' => 13,
                'province' => 'Kalimantan Selatan',
            ),
            13 => 
            array (
                'province_id' => 14,
                'province' => 'Kalimantan Tengah',
            ),
            14 => 
            array (
                'province_id' => 15,
                'province' => 'Kalimantan Timur',
            ),
            15 => 
            array (
                'province_id' => 16,
                'province' => 'Kalimantan Utara',
            ),
            16 => 
            array (
                'province_id' => 17,
                'province' => 'Kepulauan Riau',
            ),
            17 => 
            array (
                'province_id' => 18,
                'province' => 'Lampung',
            ),
            18 => 
            array (
                'province_id' => 19,
                'province' => 'Maluku',
            ),
            19 => 
            array (
                'province_id' => 20,
                'province' => 'Maluku Utara',
            ),
            20 => 
            array (
                'province_id' => 21,
            'province' => 'Nanggroe Aceh Darussalam (NAD)',
            ),
            21 => 
            array (
                'province_id' => 22,
            'province' => 'Nusa Tenggara Barat (NTB)',
            ),
            22 => 
            array (
                'province_id' => 23,
            'province' => 'Nusa Tenggara Timur (NTT)',
            ),
            23 => 
            array (
                'province_id' => 24,
                'province' => 'Papua',
            ),
            24 => 
            array (
                'province_id' => 25,
                'province' => 'Papua Barat',
            ),
            25 => 
            array (
                'province_id' => 26,
                'province' => 'Riau',
            ),
            26 => 
            array (
                'province_id' => 27,
                'province' => 'Sulawesi Barat',
            ),
            27 => 
            array (
                'province_id' => 28,
                'province' => 'Sulawesi Selatan',
            ),
            28 => 
            array (
                'province_id' => 29,
                'province' => 'Sulawesi Tengah',
            ),
            29 => 
            array (
                'province_id' => 30,
                'province' => 'Sulawesi Tenggara',
            ),
            30 => 
            array (
                'province_id' => 31,
                'province' => 'Sulawesi Utara',
            ),
            31 => 
            array (
                'province_id' => 32,
                'province' => 'Sumatera Barat',
            ),
            32 => 
            array (
                'province_id' => 33,
                'province' => 'Sumatera Selatan',
            ),
            33 => 
            array (
                'province_id' => 34,
                'province' => 'Sumatera Utara',
            ),
        ));
        
        
    }
}