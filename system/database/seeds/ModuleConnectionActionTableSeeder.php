<?php

use Illuminate\Database\Seeder;

class ModuleConnectionActionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('module_connection_action')->delete();
        
        \DB::table('module_connection_action')->insert(array (
            0 => 
            array (
                'conn_ma_module' => 'configuration',
                'conn_ma_action' => 'configuration_create',
            ),
            1 => 
            array (
                'conn_ma_module' => 'configuration',
                'conn_ma_action' => 'configuration_read',
            ),
            2 => 
            array (
                'conn_ma_module' => 'configuration',
                'conn_ma_action' => 'configuration_update',
            ),
            3 => 
            array (
                'conn_ma_module' => 'configuration',
                'conn_ma_action' => 'configuration_delete',
            ),
            4 => 
            array (
                'conn_ma_module' => 'action',
                'conn_ma_action' => 'action_create',
            ),
            5 => 
            array (
                'conn_ma_module' => 'action',
                'conn_ma_action' => 'action_list',
            ),
            6 => 
            array (
                'conn_ma_module' => 'action',
                'conn_ma_action' => 'action_update',
            ),
            7 => 
            array (
                'conn_ma_module' => 'action',
                'conn_ma_action' => 'action_delete',
            ),
            8 => 
            array (
                'conn_ma_module' => 'category',
                'conn_ma_action' => 'category_create',
            ),
            9 => 
            array (
                'conn_ma_module' => 'category',
                'conn_ma_action' => 'category_list',
            ),
            10 => 
            array (
                'conn_ma_module' => 'category',
                'conn_ma_action' => 'category_update',
            ),
            11 => 
            array (
                'conn_ma_module' => 'category',
                'conn_ma_action' => 'category_delete',
            ),
            12 => 
            array (
                'conn_ma_module' => 'customer',
                'conn_ma_action' => 'customer_create',
            ),
            13 => 
            array (
                'conn_ma_module' => 'customer',
                'conn_ma_action' => 'customer_list',
            ),
            14 => 
            array (
                'conn_ma_module' => 'customer',
                'conn_ma_action' => 'customer_update',
            ),
            15 => 
            array (
                'conn_ma_module' => 'customer',
                'conn_ma_action' => 'customer_delete',
            ),
            16 => 
            array (
                'conn_ma_module' => 'group_module',
                'conn_ma_action' => 'group_module_create',
            ),
            17 => 
            array (
                'conn_ma_module' => 'group_module',
                'conn_ma_action' => 'group_module_list',
            ),
            18 => 
            array (
                'conn_ma_module' => 'group_module',
                'conn_ma_action' => 'group_module_update',
            ),
            19 => 
            array (
                'conn_ma_module' => 'group_module',
                'conn_ma_action' => 'group_module_delete',
            ),
            20 => 
            array (
                'conn_ma_module' => 'group_user',
                'conn_ma_action' => 'group_user_create',
            ),
            21 => 
            array (
                'conn_ma_module' => 'group_user',
                'conn_ma_action' => 'group_user_list',
            ),
            22 => 
            array (
                'conn_ma_module' => 'group_user',
                'conn_ma_action' => 'group_user_update',
            ),
            23 => 
            array (
                'conn_ma_module' => 'group_user',
                'conn_ma_action' => 'group_user_delete',
            ),
            24 => 
            array (
                'conn_ma_module' => 'grouping',
                'conn_ma_action' => 'grouping_create',
            ),
            25 => 
            array (
                'conn_ma_module' => 'grouping',
                'conn_ma_action' => 'grouping_list',
            ),
            26 => 
            array (
                'conn_ma_module' => 'grouping',
                'conn_ma_action' => 'grouping_update',
            ),
            27 => 
            array (
                'conn_ma_module' => 'grouping',
                'conn_ma_action' => 'grouping_delete',
            ),
            28 => 
            array (
                'conn_ma_module' => 'master_team',
                'conn_ma_action' => 'master_team_create',
            ),
            29 => 
            array (
                'conn_ma_module' => 'master_team',
                'conn_ma_action' => 'master_team_list',
            ),
            30 => 
            array (
                'conn_ma_module' => 'master_team',
                'conn_ma_action' => 'master_team_update',
            ),
            31 => 
            array (
                'conn_ma_module' => 'module',
                'conn_ma_action' => 'module_create',
            ),
            32 => 
            array (
                'conn_ma_module' => 'module',
                'conn_ma_action' => 'module_list',
            ),
            33 => 
            array (
                'conn_ma_module' => 'module',
                'conn_ma_action' => 'module_update',
            ),
            34 => 
            array (
                'conn_ma_module' => 'module',
                'conn_ma_action' => 'module_delete',
            ),
            35 => 
            array (
                'conn_ma_module' => 'product',
                'conn_ma_action' => 'product_create',
            ),
            36 => 
            array (
                'conn_ma_module' => 'product',
                'conn_ma_action' => 'product_list',
            ),
            37 => 
            array (
                'conn_ma_module' => 'product',
                'conn_ma_action' => 'product_update',
            ),
            38 => 
            array (
                'conn_ma_module' => 'product',
                'conn_ma_action' => 'product_delete',
            ),
            39 => 
            array (
                'conn_ma_module' => 'segmentation',
                'conn_ma_action' => 'segmentation_create',
            ),
            40 => 
            array (
                'conn_ma_module' => 'segmentation',
                'conn_ma_action' => 'segmentation_list',
            ),
            41 => 
            array (
                'conn_ma_module' => 'segmentation',
                'conn_ma_action' => 'segmentation_update',
            ),
            42 => 
            array (
                'conn_ma_module' => 'segmentation',
                'conn_ma_action' => 'segmentation_delete',
            ),
            43 => 
            array (
                'conn_ma_module' => 'site',
                'conn_ma_action' => 'site_create',
            ),
            44 => 
            array (
                'conn_ma_module' => 'site',
                'conn_ma_action' => 'site_list',
            ),
            45 => 
            array (
                'conn_ma_module' => 'site',
                'conn_ma_action' => 'site_update',
            ),
            46 => 
            array (
                'conn_ma_module' => 'site',
                'conn_ma_action' => 'site_delete',
            ),
            47 => 
            array (
                'conn_ma_module' => 'tag',
                'conn_ma_action' => 'tag_create',
            ),
            48 => 
            array (
                'conn_ma_module' => 'tag',
                'conn_ma_action' => 'tag_list',
            ),
            49 => 
            array (
                'conn_ma_module' => 'tag',
                'conn_ma_action' => 'tag_update',
            ),
            50 => 
            array (
                'conn_ma_module' => 'tag',
                'conn_ma_action' => 'tag_delete',
            ),
            51 => 
            array (
                'conn_ma_module' => 'team',
                'conn_ma_action' => 'team_create',
            ),
            52 => 
            array (
                'conn_ma_module' => 'team',
                'conn_ma_action' => 'team_list',
            ),
            53 => 
            array (
                'conn_ma_module' => 'team',
                'conn_ma_action' => 'team_update',
            ),
            54 => 
            array (
                'conn_ma_module' => 'team',
                'conn_ma_action' => 'team_delete',
            ),
            55 => 
            array (
                'conn_ma_module' => 'unit',
                'conn_ma_action' => 'unit_create',
            ),
            56 => 
            array (
                'conn_ma_module' => 'unit',
                'conn_ma_action' => 'unit_list',
            ),
            57 => 
            array (
                'conn_ma_module' => 'unit',
                'conn_ma_action' => 'unit_update',
            ),
            58 => 
            array (
                'conn_ma_module' => 'unit',
                'conn_ma_action' => 'unit_delete',
            ),
        ));
        
        
    }
}