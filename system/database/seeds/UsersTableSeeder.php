<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'user_id' => 'U201808001',
                'nik' => 'U201801001',
                'name' => 'itok toni laksono',
                'email' => 'itok.toni@gmail.com',
                'password' => '$2y$10$W1sDOE45BuiXL3g42lvhs.p6HISJynvw4HpZakHP0bIR/c1wyLuVu',
                'username' => 'itoktoni',
                'photo' => 'itoktoni.jpeg',
                'active' => 1,
                'group_user' => 'developer',
                'remember_token' => 'SM9YXEuDoQkgQXTU2i3rjePuAToijlUefsul80u23e0e2ztSTqfIXX12UArr',
                'created_at' => '2017-02-01 04:39:15',
                'updated_at' => '2017-11-30 01:19:59',
                'warehouse' => 'jakarta',
                'site_id' => 'JBT',
                'target' => 100.0,
                'pencapaian' => 20.0,
                'gender' => '',
                'address' => 'Jl tanah koja RT.001/02 No. 65 Duri kosambi, Cengkareng, Jakarta Barat',
                'birth' => '2017-03-23',
                'place_birth' => 'Blora',
                'biografi' => 'Barang siapa yg mengenal dirinya maka sungguh dia telah mengenal Tuhannya.',
                'handphone' => '081311007076',
                'no_tax' => '123456',
                'created_by' => NULL,
                'sales_responsible' => 'itok.toni@gmail.com',
            ),
            1 => 
            array (
                'user_id' => 'U201808002',
                'nik' => 'test',
                'name' => 'web itok',
                'email' => 'webitok@gmail.com',
                'password' => '$2y$10$KXlya2yIkFoyHTtUW3mmAuLhqVb0oc1bwFBlvZC7R6XFkLGy3q4we',
                'username' => 'webitok',
                'photo' => NULL,
                'active' => 0,
                'group_user' => 'test',
                'remember_token' => 'XO2hgw9QytAN7tfWJNdVhQRIyQZ2MrIgRVpLZip88s3IUNfJYJGPMAi8I9eB',
                'created_at' => '2018-08-11 16:04:37',
                'updated_at' => '2018-08-18 09:17:26',
                'warehouse' => NULL,
                'site_id' => 'JBT',
                'target' => 0.0,
                'pencapaian' => 0.0,
                'gender' => '',
                'address' => '',
                'birth' => '2018-04-04',
                'place_birth' => '',
                'biografi' => '',
                'handphone' => '',
                'no_tax' => 'test',
                'created_by' => 'itoktoni',
                'sales_responsible' => '2',
            ),
            2 => 
            array (
                'user_id' => 'U201808003',
                'nik' => '',
                'name' => 'Sales',
                'email' => 'sales@sales.com',
                'password' => '$2y$10$hnibIXeErf7lzLcsQ28ML.r4LYQsMn4/yUiWzXYa3LjzFX4jZINSW',
                'username' => 'sales',
                'photo' => NULL,
                'active' => 0,
                'group_user' => 'sales',
                'remember_token' => NULL,
                'created_at' => '2018-08-18 09:24:44',
                'updated_at' => '2018-08-18 09:24:44',
                'warehouse' => NULL,
                'site_id' => 'JBT',
                'target' => NULL,
                'pencapaian' => NULL,
                'gender' => NULL,
                'address' => NULL,
                'birth' => NULL,
                'place_birth' => NULL,
                'biografi' => NULL,
                'handphone' => NULL,
                'no_tax' => '',
                'created_by' => 'itoktoni',
                'sales_responsible' => NULL,
            ),
            3 => 
            array (
                'user_id' => 'U201808004',
                'nik' => '',
                'name' => 'Operation',
                'email' => 'operation@operation.com',
                'password' => '$2y$10$JUdqZ8yAnB6Cvju1HlrmLuybF5G9tXE798A0WPKB7jalhWSPW0b4G',
                'username' => 'operation',
                'photo' => NULL,
                'active' => 0,
                'group_user' => 'operation',
                'remember_token' => NULL,
                'created_at' => '2018-08-18 09:32:21',
                'updated_at' => '2018-08-18 09:32:21',
                'warehouse' => NULL,
                'site_id' => 'JBT',
                'target' => NULL,
                'pencapaian' => NULL,
                'gender' => NULL,
                'address' => NULL,
                'birth' => NULL,
                'place_birth' => NULL,
                'biografi' => NULL,
                'handphone' => NULL,
                'no_tax' => '',
                'created_by' => 'itoktoni',
                'sales_responsible' => NULL,
            ),
            4 => 
            array (
                'user_id' => 'U201808005',
                'nik' => '',
                'name' => 'owner',
                'email' => 'owner@owner.com',
                'password' => '$2y$10$r27pu3/brmQzAyZjgdGyaeXh31IC4XReVwW83jsn4ML5qeP9j8a1O',
                'username' => 'owner',
                'photo' => NULL,
                'active' => 1,
                'group_user' => 'owner',
                'remember_token' => NULL,
                'created_at' => '2018-08-18 09:43:50',
                'updated_at' => '2018-08-18 09:44:02',
                'warehouse' => NULL,
                'site_id' => 'JBT',
                'target' => 0.0,
                'pencapaian' => 0.0,
                'gender' => '',
                'address' => '',
                'birth' => '2018-04-04',
                'place_birth' => '',
                'biografi' => '',
                'handphone' => '',
                'no_tax' => '',
                'created_by' => 'itoktoni',
                'sales_responsible' => 'U201808001',
            ),
        ));
        
    }
}