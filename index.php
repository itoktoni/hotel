<?php

require __DIR__.'/system/bootstrap/autoload.php';
$app = require_once __DIR__.'/system/bootstrap/app.php';

$app->bind('path.public', function() {
    return base_path('public'); // realpath
});

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);
$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$response->send();
$kernel->terminate($request, $response);
